﻿using System;
using Random = UnityEngine.Random;


public static class CustomUtility
{
    public static T RandomValue<T>(this T[] soure) where T : IConvertible   //enum
    {
        if (!typeof(T).IsEnum)
            throw new ArgumentException("T must be an enumerated type");

        return (T)soure.GetValue(Random.Range(0, soure.Length - 1));
    }
}
