﻿using System;
using UnityEngine;


[CreateAssetMenu]
public class ContentStorage : ScriptableObject
{
    [Header("Fireman team")]
    [SerializeField] FiremanJumper FiremanJumperPrefab = null;
    [SerializeField] FiremanPorters FiremenPortersPrefab = null;
    [SerializeField] FiremanKicker FiremanKickerPrefab = null;

    [Header("Level")]
    [SerializeField] BurningMan[] burningMans = null;

    static readonly ResourceAsset<ContentStorage> asset = new ResourceAsset<ContentStorage>("Game/ContentStorage");


    public static BurningMan GetBurningManByType(BurningMan.Type type)
    {
        return Array.Find(asset.Value.burningMans, element => element.ManType == type);
    }


    public static FiremanJumper GetFiremanJumper()
    {
        return asset.Value.FiremanJumperPrefab;
    }


    public static FiremanPorters GetFiremenPorters()
    {
        return asset.Value.FiremenPortersPrefab;
    }


    public static FiremanKicker GetFiremanKicker()
    {
        return asset.Value.FiremanKickerPrefab;
    }
}
