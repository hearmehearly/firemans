﻿using System;
using UnityEngine;


public class CollidableObject : MonoBehaviour
{
    public enum Type
    {
        None = 0,
        Tramplin = 1,
        Jumper = 2,
        Ground = 3,
        BurningMan = 4
    }

    public event Action<Collision2D, Type> OnCustomCollisionEnter2D;
    public event Action<Collider2D, Type> OnCustomTriggerEnter2D;

    [SerializeField] Type type = Type.None;


    void OnCollisionEnter2D(Collision2D collision)
    {
        Type anotherObjectType = Type.None;
        CollidableObject anotherObject = collision.gameObject.GetComponent<CollidableObject>();

        if (anotherObject)
        {
            anotherObjectType = anotherObject.type;
        }

        OnCustomCollisionEnter2D?.Invoke(collision, anotherObjectType);
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        Type anotherObjectType = Type.None;
        CollidableObject anotherObject = collision.gameObject.GetComponent<CollidableObject>();

        if (anotherObject)
        {
            anotherObjectType = anotherObject.type;
        }
        
        OnCustomTriggerEnter2D?.Invoke(collision, anotherObjectType);
    }
}
