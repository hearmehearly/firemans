﻿using System;
using UnityEngine;


public class Timer
{
    const string STARTTIME = "_start_time";
    const string FINISHTIME = "_finish_time";

    readonly string key;

    Action onTimeOverCallback;

    bool shouldCheckForStop;




    public TimeSpan TimeLeft => FinishTime.Subtract(DateTime.UtcNow);


    public DateTime StartTime => CustomPlayerPrefs.GetDateTime(StartTimeKey, DateTime.UtcNow);


    public DateTime FinishTime => CustomPlayerPrefs.GetDateTime(FinishTimeKey, DateTime.UtcNow);


    public bool IsTimeOver => TimeLeft.TotalSeconds < 0.0d;


    public bool IsTimerActive => PlayerPrefs.HasKey(StartTimeKey) && PlayerPrefs.HasKey(FinishTimeKey);


    string StartTimeKey => string.Concat(key, STARTTIME);


    string FinishTimeKey => string.Concat(key, FINISHTIME);



    public Timer(string timerKey)
    {
        key = timerKey;
    }


    public void Initialize()
    {
        Scheduler.OnUpdate += UpdateTimer;
        shouldCheckForStop = true;
    }


    public void Start(float duration)
    {
        if (IsTimerActive)
        {
            Debug.LogError("Trying to Start already active timer with key " + key);
        }

        SetUpStartTime();
        SetUpFinishTime(duration);
        Initialize();
    }


    public void SetTimeOverCallback(Action callback)
    {
        onTimeOverCallback = callback;
    }


    public void Stop()
    {
        Scheduler.OnUpdate -= UpdateTimer;
        shouldCheckForStop = false;

        PlayerPrefs.DeleteKey(StartTimeKey);
        PlayerPrefs.DeleteKey(FinishTimeKey);
    }


    void SetUpStartTime()
    {
        CustomPlayerPrefs.SetDateTime(StartTimeKey, DateTime.UtcNow);
    }


    void SetUpFinishTime(float seconds)
    {
        CustomPlayerPrefs.SetDateTime(FinishTimeKey, DateTime.UtcNow.AddSeconds(seconds));
    }


    void UpdateTimer()
    {
        if (shouldCheckForStop && IsTimeOver)
        {
            Stop();
            onTimeOverCallback?.Invoke();
        }
    }
}
