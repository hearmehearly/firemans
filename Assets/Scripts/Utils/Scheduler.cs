﻿using System.Collections;
using UnityEngine;
using System;


public class Scheduler : MonoBehaviour
{
    public static event Action OnGameStart;
    public static event Action OnGameQuit;
    public static event Action OnUpdate;
    public static event Action OnFixedUpdate;

    static Scheduler instance;
    

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Initialize()
    {
        GameObject go = new GameObject("Monobehavior Sheduler");
        instance = go.AddComponent<Scheduler>();
        DontDestroyOnLoad(go);
    }

    public static Coroutine PlayCoroutine(IEnumerator routine)
    {
        return instance.StartCoroutine(routine);
    }


    public static void StopPlayingCoroutine(Coroutine routine)
    {
        instance.StopCoroutine(routine);
    }


    public static Coroutine PlayMethodWithDelay(Action method, float delay)
    {
        return instance.StartCoroutine(instance.InvokeMethodWithDelay(delay, method));
    }


    void Awake()
    {
        OnGameStart?.Invoke();
    }


    void Update()
    {
        OnUpdate?.Invoke();
    }


    void FixedUpdate()
    {
        OnFixedUpdate?.Invoke();
    }


    void OnApplicationQuit()
    {
        OnGameQuit?.Invoke();
    }


    IEnumerator InvokeMethodWithDelay(float delay, Action callback)
    {
        yield return new WaitForSeconds(delay);
        callback?.Invoke();
    }
}
