﻿using UnityEngine;


public class GameManager : MonoBehaviour
{
    private SceneArena sceneArena;

    public static GameManager Instance
    {
        get;
        private set;
    }


    private void Awake()
    {
        Instance = this;
        Input.multiTouchEnabled = false;
    }


    private void Start()
    {
        sceneArena = GetComponent<SceneArena>();

        StartGame();
    }


    public void PauseGame()
    {
        Time.timeScale = 0.0f;
    }


    public void UnpauseGame()
    {
        Time.timeScale = 1.0f;
    }


    private void StartGame()
    {
        //PlayArena();
        ShowScenePlayer();
    }


    private void PlayArena()
    {
        sceneArena.Play(ShowScenePlayer);
    }


    private void ShowScenePlayer()
    {
        UIPlayerMenu.Prefab.Instance.Show((_) => PlayArena());
    }


    private void OnGUI()
    {
        if (GUI.Button(new Rect(150, 150, 150, 150), "GOLD"))
        {
            Player.AddCoins(100);
        }
    }
}
