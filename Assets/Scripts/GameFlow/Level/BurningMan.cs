﻿using UnityEngine;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;


public class BurningMan : MonoBehaviour
{
    public enum Type
    {
        None = 0,
        SimpleMan = 1,
        SimpleWoman = 2,
        HardMan = 3,
        HardWoman = 4,
        Cat = 5
    }


    public enum State
    {
        None = 0,
        InWindow = 1,
        Attached = 2,
        Throwing = 3,
        Freed = 4
    }


    public event Action OnDisappear;

    [Header("Entity")]
    [SerializeField]
    private Type type = Type.None;
    [SerializeField]
    private SpriteRenderer burnSlider = null;
    [SerializeField]
    private SpriteRenderer spriteRenderer = null;
    [SerializeField]
    private CollidableObject collidableObject = null;

    [Header("External Components")]
    [SerializeField]
    private BurningManAnimationInfo animationInfo = null;
    [SerializeField]
    private BurningManSettings settings = null;

    private Tween burnTween;



    public State CurrentState
    {
        get;
        private set;
    }

    public bool CanBeAttached => !IsFreed && !IsThrowing;
    

    public Type ManType => type;


    public float AdditionalJumpPower => settings.additionalJumpPower;


    bool IsThrowing => CurrentState == State.Throwing;


    bool IsFreed => CurrentState == State.Freed;



    private void OnEnable()
    {
        collidableObject.OnCustomCollisionEnter2D += ProcessCollision;
    }


    private void OnDisable()
    {
        collidableObject.OnCustomCollisionEnter2D -= ProcessCollision;
    }


    public void Initialize(float targetBurnDuration)
    {
        CurrentState = State.InWindow;

        float endValue = burnSlider.size.x;
        burnTween = DOTween.To(() => endValue,
                            x => burnSlider.size = new Vector2(x, burnSlider.size.y),
                            0.0f,
                            targetBurnDuration)
                            .SetEase(Ease.Linear)
                            .OnComplete(Throw);


        spriteRenderer.sprite = settings.normalSprite;
    }


    public void AttachToJumper(Transform anchor)
    {
        burnSlider.gameObject.SetActive(false);
        burnTween.Kill();

        CurrentState = State.Attached;

        transform.position = anchor.position;
        transform.SetParent(anchor);
    }


    public void MakeFree()
    {
        CurrentState = State.Freed;

        transform.SetParent(null);

        int direction = Random.value > 0.5 ? 1 : -1;
        PlayJumpAnimation(() => PlayRunAnimation(Disappear));

        void PlayJumpAnimation(Action callback)
        {
            Vector3 destination = new Vector3(transform.position.x + direction * animationInfo.OffsetXForJumping,
                                              animationInfo.EndPositionY, transform.position.z);

            transform
                .DOMoveX(destination.x, animationInfo.JumpDuration)
                .SetEase(animationInfo.JumpCurveX);

            transform
                .DOMoveY(destination.y, animationInfo.JumpDuration)
                .SetEase(animationInfo.JumpCurveY)
                .OnComplete(() => callback?.Invoke());
        }


        void PlayRunAnimation(Action callback)
        {
            float distanceToRun = transform.position.x + direction * animationInfo.DistanceXToRunAfterJump;

            transform
                .DOMoveX(distanceToRun, animationInfo.RunDuration)
                .SetEase(animationInfo.RunCurve)
                .OnComplete(() => callback?.Invoke());
        }
    }


    public void PlayDieWithFiremanAnimation()
    {
        MakeFree();
    }


    private void Disappear()
    {
        Destroy(gameObject);
        OnDisappear?.Invoke();
    }


    private void Throw()
    {
        CurrentState = State.Throwing;

        Vector3 destination = new Vector3(transform.position.x, animationInfo.EndThrowPositionY);

        transform
            .DOMove(destination, Vector3.Distance(transform.position, destination) / animationInfo.ThrowSpeed)
            .SetEase(animationInfo.ThrowCurve)
            .OnComplete(Disappear);

        spriteRenderer.sprite = settings.throwingSprite;
    }


    private void ProcessCollision(Collision2D collision, CollidableObject.Type collisionType)
    {
    }
}