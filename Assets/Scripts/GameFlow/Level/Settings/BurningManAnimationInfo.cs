﻿using UnityEngine;


[CreateAssetMenu]
public class BurningManAnimationInfo : ScriptableObject
{
    [Header("Jump settings")]
    public float EndPositionY = 0.0f;
    public float OffsetXForJumping = 0.0f;
    public float JumpDuration = 0.0f;

    public AnimationCurve JumpCurveX = null;
    public AnimationCurve JumpCurveY = null;

    public float DistanceXToRunAfterJump = 0.0f;
    public float RunDuration = 0.0f;
    public AnimationCurve RunCurve = null;

    [Header("Throw settings")]
    public float EndThrowPositionY = 0.0f;
    public AnimationCurve ThrowCurve = null;
    public float ThrowSpeed = 0.0f;
}
