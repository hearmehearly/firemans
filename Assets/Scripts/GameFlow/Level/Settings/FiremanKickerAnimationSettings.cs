﻿using UnityEngine;


[CreateAssetMenu]
public class FiremanKickerAnimationSettings : ScriptableObject
{
    public AnimationCurve MoveCurve = null;
    public float MoveDuration = 0.0f;
    public Vector3 StartFromPosition = Vector3.zero;
}
