﻿using UnityEngine;
using TMPro;
using DG.Tweening;


public class LevelCoinsController : MonoBehaviour
{
    private const string RECEIVED_COINS_FORMAT = "+ {0}";

    [Header("Components")]
    [SerializeField]
    private TextMeshPro receivedCoincsPrefab = null;

    [Header("Animation")]
    [SerializeField]
    private float fadeDuration = 0.0f;
    [SerializeField]
    private AnimationCurve fadeCurve = null;
    [SerializeField]
    private float moveYOffset = 0.0f;
    [SerializeField]
    private float moveDuration = 0.0f;
    [SerializeField]
    private AnimationCurve moveCurve = null;


    private void OnDisable()
    {
        DOTween.Kill(this);
    }



    public void ShowReceivedCoinsEffect(float coins, Vector3 position)
    {
        TextMeshPro receivedTextEffect = Instantiate(receivedCoincsPrefab, position, Quaternion.identity);
        receivedTextEffect.text = string.Format(RECEIVED_COINS_FORMAT, coins);

        receivedTextEffect
            .DOFade(0.0f, fadeDuration)
            .SetEase(fadeCurve)
            .OnComplete(() => Destroy(receivedTextEffect.gameObject))
            .SetId(this);

        receivedTextEffect.transform
            .DOLocalMoveY(position.y + moveYOffset, moveDuration)
            .SetEase(moveCurve)
            .SetId(this);
    }
}
