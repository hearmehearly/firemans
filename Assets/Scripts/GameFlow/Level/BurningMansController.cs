﻿using System.Collections.Generic;
using UnityEngine;


public class BurningMansController : MonoBehaviour
{
    List<BurningMan> currentBurningMans = new List<BurningMan>();

    public void CreateBurningMans()
    {
        BurningManSpawnSettings[] levelBurningManSettings = Levels.GetLevelBurningManSettings(Player.Level);

        foreach (var setting in levelBurningManSettings)
        {
            Scheduler.PlayMethodWithDelay(() => SpawnBurningMan(setting), setting.spawnDelay);
        }
    }


    public void DestroyBurningMans()
    {
        RefreshBurningMans();

        foreach (BurningMan burningMan in currentBurningMans)
        {
            Destroy(burningMan.gameObject);
        }
    }


    private void SpawnBurningMan(BurningManSpawnSettings setting)
    {
        BurningMan.Type randomType = setting.availableTypes.RandomValue();
        BurningMan burningMan = Instantiate(ContentStorage.GetBurningManByType(randomType), setting.spawnPosition, Quaternion.identity, transform);

        currentBurningMans.Add(burningMan);

        burningMan.Initialize(setting.BurnDuration);
        burningMan.OnDisappear += RefreshBurningMans;
    }


    private void RefreshBurningMans()
    {
        currentBurningMans.RemoveAll(element => element == null);
    }
}
