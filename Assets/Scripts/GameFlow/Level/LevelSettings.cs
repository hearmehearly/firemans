﻿using System;
using UnityEngine;


[Serializable]
public struct BurningManSpawnSettings
{
    public Vector3 spawnPosition;
    public BurningMan.Type[] availableTypes;
    public float spawnDelay;
    public float BurnDuration;
}


[CreateAssetMenu]
public class LevelSettings : ScriptableObject
{
    public BurningManSpawnSettings[] burningManSpawnSettings = null;

    public string Name = null;
    public GameObject Background = null;
    public float Duration = 0.0f;

    public float CoinsForWin = 0f;
    public float CoinsForBurningMan = 0f;
    public float UnlockPrice = 0f;

    void OnValidate()
    {
        for (int i = 0; i < burningManSpawnSettings.Length; i++)
        {
            burningManSpawnSettings[i].BurnDuration = 55f;
        }
    }
}
