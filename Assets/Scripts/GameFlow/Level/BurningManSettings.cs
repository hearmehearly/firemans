﻿using UnityEngine;


[CreateAssetMenu(fileName = "BurningManSettings",
                 menuName = "Settings/Level/BurningManSettings")]
public class BurningManSettings : ScriptableObject
{
    public float additionalJumpPower = 0.0f;

    public Sprite normalSprite = null;
    public Sprite throwingSprite = null;
}
