﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class FiremanTeamController : MonoBehaviour
{
    // IN SEPARATE SCRIPTABLE OBJECT
    [Header("System Components")]
    [SerializeField]
    private float delayHoldHint = 0.0f;
    [SerializeField]
    private Vector3 portersSpawnPoint = Vector3.zero;
    [SerializeField]
    private Vector3 jumperSpawnPoint = Vector3.zero;

    [Header("Fucking Shit")]
    [SerializeField]
    private LevelCoinsController coinsController = null;

    FiremanJumper jumper;
    FiremanPorters porters;
    FiremanKicker kicker;

    private readonly List<Fireman> currentFiremans = new List<Fireman>();

    private Action<float> levelWinCallback;
    private Action<float> levelLoseCallback;



    private float TotalEarnedCoins
    {
        get
        {
            float result = 0.0f;

            foreach (Fireman fireman in currentFiremans)
            {
                result += fireman.EarnedLevelCoins;
            }

            return result;
        }
    }



    public void CreateFiremans()
    {
        jumper = Instantiate(ContentStorage.GetFiremanJumper(), jumperSpawnPoint, Quaternion.identity, transform);
        jumper.DisableCollision();
        jumper.SetData(Player.FiremansData.jumperData);

        porters = Instantiate(ContentStorage.GetFiremenPorters(), portersSpawnPoint, Quaternion.identity, transform);
        porters.SetData(Player.FiremansData.portersData);

        kicker = Instantiate(ContentStorage.GetFiremanKicker(), transform);
        kicker.SetData(Player.FiremansData.kickerData);

        currentFiremans.Add(jumper);
        currentFiremans.Add(porters);
        currentFiremans.Add(kicker);
    }


    public void PlayStartFiremansAnimation()
    {
        kicker.AddForceToRigidBody(jumper.MainRigidbody2D, () =>
        {
            jumper.EnableCollision();

            Scheduler.PlayMethodWithDelay(() =>
            {
                UIHint.Prefab.Instance.Show(UIHint.Type.Hold, (_) =>
                {
                    porters.StartLevel();
                }, GameManager.Instance.PauseGame);
            }, delayHoldHint);
        });
    }


    public void DestroyFiremans()
    {
        foreach (Fireman fireman in currentFiremans)
        {
            Destroy(fireman.gameObject);
        }

        currentFiremans.Clear();
    }
    

    public void FinishLevel()
    {
        jumper.FinishLevel();
        porters.FinishLevel();
    }


    public void StartLevel(Action<float> winCallback, Action<float> loseCallback)
    {
        levelWinCallback = winCallback;
        levelLoseCallback = loseCallback;

        foreach (Fireman fireman in currentFiremans)
        {
            fireman.OnCoinsReceived += OnCoinsReceived;
        }

        UILevel.Prefab.Instance.SetCoins(0.0f);

        jumper.OnAllBurningMansReceived += TriggerWinCallback;
        jumper.OnDie += TriggerLoseCallback;
    }


    private void OnCoinsReceived(float coins, Vector3 position)
    {
        coinsController.ShowReceivedCoinsEffect(coins, position);

        UILevel.Prefab.Instance.SetCoins(TotalEarnedCoins);
    }


    private void TriggerWinCallback()
    {
        levelWinCallback?.Invoke(TotalEarnedCoins);
    }


    private void TriggerLoseCallback()
    {
        levelLoseCallback?.Invoke(TotalEarnedCoins);
    }
}
