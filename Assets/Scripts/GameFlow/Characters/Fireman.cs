﻿using System;
using UnityEngine;


public abstract class Fireman : MonoBehaviour
{
    public event Action<float, Vector3> OnCoinsReceived;

    public float EarnedLevelCoins
    {
        get;
        set;
    }

    //  public abstract void SetData();

    public void TriggerReceiveCoinsEvent(float coins, Vector3 position)
    {
        OnCoinsReceived?.Invoke(coins, position);
    }
}
