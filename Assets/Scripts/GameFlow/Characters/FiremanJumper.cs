﻿using DG.Tweening;
using System;
using UnityEngine;


public class FiremanJumper : Fireman
{
    [Serializable]
    public class Data
    {
        public float JumpPower = 0f;
    }

    public event Action OnAllBurningMansReceived = null;
    public event Action OnDie = null;

    [Header("Entity")]
    [SerializeField]
    private Rigidbody2D mainRigidbody2D = null;
    [SerializeField]
    private Collider2D mainCollider2D = null;

    [SerializeField]
    private CollidableObject jumperCollidableObject = null;
    [SerializeField]
    private Transform burningManAnchor = null;

    private BurningMan attachedBurningMan;


    public Data CurrentData
    {
        get;
        private set;
    }


    public int SavedBurningMans
    {
        get;
        private set;
    }

    public Rigidbody2D MainRigidbody2D => mainRigidbody2D;



    private void OnEnable()
    {
        jumperCollidableObject.OnCustomCollisionEnter2D += JumperCollidableObject_OnCustomCollisionEnter2D;
        jumperCollidableObject.OnCustomTriggerEnter2D += JumperCollidableObject_OnCustomTriggerEnter2D;
    }


    private void OnDisable()
    {
        jumperCollidableObject.OnCustomCollisionEnter2D -= JumperCollidableObject_OnCustomCollisionEnter2D;
        jumperCollidableObject.OnCustomTriggerEnter2D -= JumperCollidableObject_OnCustomTriggerEnter2D;
    }


    public void FinishLevel()
    {
        OnAllBurningMansReceived = null;
        OnDie = null;
    }
    

    public void EnableCollision()
    {
        mainCollider2D.enabled = true;
        MainRigidbody2D.isKinematic = false;
    }


    public void DisableCollision()
    {
        mainCollider2D.enabled = false;
        MainRigidbody2D.isKinematic = true;
    }


    public void SetData(Data targetData)
    {
        CurrentData = targetData;
    }


    public void SetVelocityAngle(float angle)
    {
        float angleRadian = angle * Mathf.Deg2Rad;
        float savedVelocityMagnitude = mainRigidbody2D.velocity.magnitude;

        mainRigidbody2D.velocity = new Vector2
        {
            x = Mathf.Cos(angleRadian) * savedVelocityMagnitude,
            y = Mathf.Sin(angleRadian) * savedVelocityMagnitude
        };
    }


    private void JumperCollidableObject_OnCustomCollisionEnter2D(Collision2D collision, CollidableObject.Type type)
    {
        switch (type)
        {
            case CollidableObject.Type.None:
                break;

            case CollidableObject.Type.Tramplin:
                MakeFreeAttacedhBurningMan();
                AddJumpPower(CurrentData.JumpPower);
                break;

            case CollidableObject.Type.Jumper:
                break;

            case CollidableObject.Type.Ground:
                Die();
                break;

            case CollidableObject.Type.BurningMan:
                AttachBurningMan(collision.gameObject.GetComponent<BurningMan>());
                break;

            default:
                throw new NotImplementedException(type.ToString());
        }
    }


    private void JumperCollidableObject_OnCustomTriggerEnter2D(Collider2D collision, CollidableObject.Type type)
    {
        if (type == CollidableObject.Type.BurningMan)
        {
            AttachBurningMan(collision.gameObject.GetComponent<BurningMan>());
        }
    }


    private void Die()
    {
        DisableCollision();
        mainRigidbody2D.velocity = Vector3.zero;
        transform.DORotate(new Vector3(0.0f, 0.0f, 90.0f), 0.5f);

        if (attachedBurningMan != null)
        {
            attachedBurningMan.PlayDieWithFiremanAnimation();
        }

        OnDie?.Invoke();
    }


    private void AttachBurningMan(BurningMan burningMan)
    {
        if (attachedBurningMan == null && burningMan.CanBeAttached)
        {
            attachedBurningMan = burningMan;
            burningMan.AttachToJumper(burningManAnchor);
        }
    }


    private void MakeFreeAttacedhBurningMan()
    {
        if (attachedBurningMan == null)
        {
            return;
        }

        AddJumpPower(attachedBurningMan.AdditionalJumpPower);

        attachedBurningMan.MakeFree();
        attachedBurningMan = null;

        SavedBurningMans++;
        UILevel.Prefab.Instance.SetSavedMansInfo(SavedBurningMans);

        int burningMansOnLevel = Levels.GetLevelBurningManSettings(Player.Level).Length;

        float coinsForSave = Levels.GetCoinsForBurningMan(Player.Level);
        EarnedLevelCoins += coinsForSave;

        TriggerReceiveCoinsEvent(coinsForSave, transform.position);

        if (SavedBurningMans == burningMansOnLevel)
        {
            OnAllBurningMansReceived?.Invoke();
        }
    }


    private void AddJumpPower(float additionalJumpPower)
    {
#warning REFRESH LOGIC FOR VELOCITY
        if (mainRigidbody2D.velocity.y < 30)
        mainRigidbody2D.velocity += mainRigidbody2D.velocity.normalized * additionalJumpPower;
    }

    private void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 150, 150), "QWEQWE"))
        {
            float coinsForSave = Levels.GetCoinsForBurningMan(Player.Level);
            EarnedLevelCoins += coinsForSave;
            TriggerReceiveCoinsEvent(Levels.GetCoinsForBurningMan(Player.Level), transform.position);

        }
    }
}
