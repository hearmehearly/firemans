﻿using UnityEngine;
using DG.Tweening;
using System;


public class FiremanPorters : Fireman
{
    [Serializable]
    public class Data
    {
        public const float BaseVelocity = 2f;
        public float Speed = 0f;
    }


    [SerializeField]
    private CollidableObject trampolin = null;

    private Data currentData;



    public void SetData(Data data)
    {
        currentData = data;
    }


    public void StartLevel()
    {
        InputController.OnMouseDown += MoveFireman;
        trampolin.OnCustomCollisionEnter2D += Trampolin_OnCustomCollisionEnter2D;
    }


    public void FinishLevel()
    {
        InputController.OnMouseDown -= MoveFireman;
        trampolin.OnCustomCollisionEnter2D -= Trampolin_OnCustomCollisionEnter2D;

        transform.DOKill();
    }


    private void Trampolin_OnCustomCollisionEnter2D(Collision2D collision, CollidableObject.Type type)
    {
        if (type == CollidableObject.Type.Jumper)
        {
            FiremanJumper jumper = collision.gameObject.GetComponent<FiremanJumper>();
            float angle = Vector2.SignedAngle(jumper.MainRigidbody2D.velocity, trampolin.transform.right);

            Debug.Log(angle);
            if (Mathf.Abs(angle) == 90.0f)
            {
                jumper.SetVelocityAngle(45.0f);
            }
        }
    }


    private void MoveFireman(Vector3 pressedPosition)
    {
        transform.DOKill();
        
        float duration = Vector3.Distance(transform.position, new Vector3(pressedPosition.x, transform.position.y, transform.position.z)) / PlayerConfig.GetPortersSpeed();

        transform
            .DOMoveX(pressedPosition.x, duration)
            .SetEase(Ease.Linear);
    }
}
