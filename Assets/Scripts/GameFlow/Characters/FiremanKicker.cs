﻿using DG.Tweening;
using System;
using UnityEngine;


public class FiremanKicker : Fireman
{
    [Serializable]
    public class Data
    {
        public float KickForce = 1f;
    }

    public readonly Vector2 BaseVelocity = new Vector2(0.0f, 6.0f);

    [SerializeField]
    private Collider2D mainCollider2D = null;

    [SerializeField]
    private FiremanKickerAnimationSettings animationSettings = null;



    public Data CurrentData
    {
        get;
        private set;
    }



    public void SetData(Data targetData)
    {
        CurrentData = targetData;
    }


    public void AddForceToRigidBody(Rigidbody2D rigidbody2D, Action onForceAdded)
    {
        transform.position = animationSettings.StartFromPosition;
        Vector3 destination = new Vector3(rigidbody2D.transform.position.x, transform.position.y, transform.position.z);

        transform
        .DOMove(destination, animationSettings.MoveDuration)
        .SetEase(animationSettings.MoveCurve)
        .OnComplete(() =>
        {
            UIHint.Prefab.Instance.Show(UIHint.Type.Press, (_) =>
            {
                GiveForce();
                Leave();
            });
        });

        void GiveForce()
        {
            rigidbody2D.velocity += BaseVelocity + rigidbody2D.velocity.normalized * CurrentData.KickForce;
            onForceAdded?.Invoke();
        }

        void Leave()
        {
            transform
                .DOMove(animationSettings.StartFromPosition, animationSettings.MoveDuration)
                .SetEase(animationSettings.MoveCurve);
        }
    }
}
