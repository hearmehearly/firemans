﻿using System;


public class FiremanHealer : Fireman
{
    [Serializable]
    public class Data
    {
        public bool IsBought = false;
        public int ResqueLives = 0;
    }

    private Data currentData;


    private void RefreshData()
    {
        currentData = Player.FiremansData.healerData;
    }
}
