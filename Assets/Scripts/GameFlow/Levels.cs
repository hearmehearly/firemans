﻿using UnityEngine;


[CreateAssetMenu]
public class Levels : ScriptableObject
{
    static readonly ResourceAsset<Levels> asset = new ResourceAsset<Levels>("Game/Levels");

    [SerializeField] LevelSettings[] configs = null;


    public static GameObject GetLevelBackground(int level)
    {
        return asset.Value.configs[level].Background;
    }


    public static BurningManSpawnSettings[] GetLevelBurningManSettings(int level)
    {
        return asset.Value.configs[level].burningManSpawnSettings;
    }


    public static string GetLevelName(int level)
    {
        return asset.Value.configs[level].Name;
    }


    public static int GetLevelsCount()
    {
        return asset.Value.configs.Length;
    }


    public static float GetCoinsForWin(int level)
    {
        return asset.Value.configs[level].CoinsForWin;
    }


    public static float GetCoinsForBurningMan(int level)
    {
        return asset.Value.configs[level].CoinsForBurningMan;
    }


    public static float GetCoinsForMinion(int level)
    {
        return asset.Value.configs[level].CoinsForBurningMan;
    }


    public static float GetUnlockPrice(int level)
    {
        return asset.Value.configs[level].UnlockPrice;
    }


    public static float GetDuration(int level)
    {
        return asset.Value.configs[level].Duration;
    }
}
