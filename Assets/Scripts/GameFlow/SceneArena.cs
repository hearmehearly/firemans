﻿using System;
using System.Collections;
using UnityEngine;


public class SceneArena : MonoBehaviour
{
    [SerializeField]
    private float delayBeforeLevelResult = 2.0f;

    [SerializeField]
    private Level arena = null;
    private Action onFinishArena;
    private Level.Result arenaResult;


    private void Start()
    {
        arena.Init();
    }


    public void Play(Action onFinish)
    {
        onFinishArena = onFinish;
        arena.Show(OnFinishArena);

        UILevel.Prefab.Instance.Show();
        UILevel.Prefab.Instance.SetLevel(Player.Level + 1);
    }


    private void OnFinishArena(Level.Result result)
    {
        arenaResult = result;
        FinishLevel();
    }


    private void FinishLevel()
    {
        StartCoroutine(ShowLevelResult());
    }


    private IEnumerator ShowLevelResult()
    {
        yield return new WaitForSeconds(delayBeforeLevelResult);

        UILevelResult.Prefab.Instance.Show(arenaResult, (_) => OnFinish(arenaResult));
    }


    private void OnFinish(Level.Result result)
    {
        if (result.Win)
        {
            Player.UpLevel();
        }

        UILevel.Prefab.Instance.Hide();
        onFinishArena();
    }
}