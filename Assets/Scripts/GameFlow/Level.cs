﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;


public class Level : Unit<Level.Result>
{
    public class Result : UnitResult
    {
        public bool Win;
        public float Coins;
    }

    public enum State
    {
        Playing,
        OutOfAmmo,
        Win,
        Lose
    }

    private readonly Dictionary<GameObject, GameObject> backs = new Dictionary<GameObject, GameObject>();

    private BurningMansController burningMansController;
    private FiremanTeamController firemanTeamController;

    private Action<float> OnAllBurningMansSave = null;
    private Action<float> OnJumperDie = null;
   


    private State CurrentState
    {
        get;
        
        set;
    }


    void Awake()
    {
        burningMansController = GetComponent<BurningMansController>();
        firemanTeamController = GetComponent<FiremanTeamController>();

        CreateBorderColliders();
    }


    public void Init()
    {
    }


    public override void Show(Action<Result> onHided, Action onShowed = null)
    {
        base.Show(onHided, onShowed);
        
        HideBack();
        ShowBack(Levels.GetLevelBackground(Player.Level));
        
        OnAllBurningMansSave = OnAllBurningMansSaved;
        OnJumperDie = OnJumperDead;

        firemanTeamController.CreateFiremans();
        firemanTeamController.PlayStartFiremansAnimation();
        firemanTeamController.StartLevel(OnAllBurningMansSave, OnJumperDie);

        burningMansController.CreateBurningMans();
    }


    protected override void Hided(Result result)
    {
        base.Hided(result);

        burningMansController.DestroyBurningMans();

        CurrentState = State.Playing;
    }


    private void OnAllBurningMansSaved(float coinsAmount)
    {
        CurrentState = State.Win;
        firemanTeamController.FinishLevel();

        StartCoroutine(FinishLevel(1f, coinsAmount));
    }


    private void OnJumperDead(float coins)
    {
        CurrentState = State.Lose;
        firemanTeamController.FinishLevel();

        StartCoroutine(FinishLevel(1f, coins));
    }


    private  IEnumerator FinishLevel(float delay, float coins)
    {
        Debug.Log("playing animation");
        yield return new WaitForSeconds(3);
        // here must be animation
        yield return new WaitForSeconds(delay);

        firemanTeamController.DestroyFiremans();

        Hided(new Result
        {
            Win = CurrentState == State.Win,
            Coins = coins
        });
    }


    private void ShowBack(GameObject prefab)
    {
        if (!backs.ContainsKey(prefab))
        {
            backs[prefab] = Instantiate(prefab, transform);
        }

        GameObject back = backs[prefab];
        back.SetActive(true);
    }


    private void HideBack()
    {
        foreach (GameObject back in backs.Values)
        {
            back.SetActive(false);
        }
    }


    private void CreateBorderColliders()
    {
        Camera GameCamera = Camera.main;
        float levelHalfWidth = GameCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, 0.0f)).x;
        float levelHalfHeight = GameCamera.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, 0.0f)).y;
        const float colliderWidth = 1.0f;

        GameObject border = new GameObject("LevelBorders");
        border.transform.SetParent(transform);

        BoxCollider2D leftCollider = border.AddComponent<BoxCollider2D>();
        leftCollider.offset = new Vector2(-levelHalfWidth - (colliderWidth / 2.0f), 0.0f);
        leftCollider.size = new Vector2(colliderWidth, levelHalfHeight * 2.0f);

        BoxCollider2D rightCollider = border.AddComponent<BoxCollider2D>();
        rightCollider.offset = new Vector2(levelHalfWidth + (colliderWidth / 2.0f), 0.0f);
        rightCollider.size = new Vector2(colliderWidth, levelHalfHeight * 2.0f);

        BoxCollider2D topCollider = border.AddComponent<BoxCollider2D>();
        topCollider.offset = new Vector2(0.0f, levelHalfHeight + (colliderWidth / 2.0f));
        topCollider.size = new Vector2(levelHalfWidth * 2.0f, colliderWidth);
    }
}
