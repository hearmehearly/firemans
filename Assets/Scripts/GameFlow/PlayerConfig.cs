﻿public static class PlayerConfig
{
    public static float JumperJumpPowerCost => Player.FiremansData.jumperData.JumpPower * 100f;


    public static float PortersSpeedCost => Player.FiremansData.portersData.Speed * 100f;
    

    public static float BuyHealerCost => 666f;
    

    public static float HealerResqueLifesCost => Player.FiremansData.healerData.ResqueLives * 100f;
    

    public static float KickerKickForceCost => Player.FiremansData.kickerData.KickForce * 100f;




    public static void UpJumperJumpPower()
    {
        //TODO: formula here
        Player.FiremansData.jumperData.JumpPower += 1f;
    }


    public static void UpPortersSpeed()
    {
        //TODO: formula here
        Player.FiremansData.portersData.Speed += 1f;
    }


    public static void BuyHealer()
    {
        Player.FiremansData.healerData.IsBought = true;
    }


    public static void UpHealerResqueLifes()
    {
        // TOO: FORMULA
        Player.FiremansData.healerData.ResqueLives += 1;
    }


    public static void UpKickerKickForce()
    {
        //TODO: formula here
        Player.FiremansData.kickerData.KickForce += 1f;
    }


    public static float GetPortersSpeed()
    {
        return Player.FiremansData.portersData.Speed + FiremanPorters.Data.BaseVelocity;
    }
}
