﻿using System;


[Serializable]
public class FiremansProgress
{
    public FiremanJumper.Data jumperData = new FiremanJumper.Data();
    public FiremanPorters.Data portersData = new FiremanPorters.Data();
    public FiremanHealer.Data healerData = new FiremanHealer.Data();
    public FiremanKicker.Data kickerData = new FiremanKicker.Data(); 
}


public static class Player
{
    [Serializable]
    public class Data
    {
        public int Level;
        public float Coins = 0.0f;
        public float Gems;

        public FiremansProgress firemansProgress = new FiremansProgress();
    }


    const string DATA_KEY = "PlayerData_New";

    static Data data;
    
    public static event Action OnChangeCoins;
    public static event Action OnChangeGems;


    public static int Level
    {
        get => data.Level;
        
        set
        {
            data.Level = value;
            CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
        }
    }


    public static float Coins
    {
        get => data.Coins;
        
        private set
        {
            data.Coins = value;
            CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
            OnChangeCoins?.Invoke();
        }
    }


    public static float Gems
    {
        get => data.Gems;
        
        private set
        {
            data.Gems = value;
            CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
            OnChangeGems();
        }
    }


    public static FiremansProgress FiremansData
    {
        get => data.firemansProgress;

        private set
        {
            data.firemansProgress = value;
            CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
        }
    }


    static Player()
    {
        data = CustomPlayerPrefs.GetObjectValue<Data>(DATA_KEY);

        if (data == null)
        {
            UnityEngine.Debug.Log("NEW GAME STARTED");
        }

        data = data ?? new Data();

        CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
    }


    public static void UpLevel()
    {
        Level++;
    }


    public static void AddCoins(float count)
    {
        Coins += count;
    }


    public static void AddGems(float gems)
    {
        Gems += gems;
    }


    public static bool TryRemoveCoins(float count)
    {
        if (Coins < count)
        {
            return false;
        }

        Coins -= count;

        return true;
    }


    public static bool TryRemoveGems(float count)
    {
        if (Gems < count)
        {
            return false;
        }

        Gems -= count;
        return true;
    }
}
