﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class UIHealerCell : UIFiremanCell
{
    private const string UI_FORMAT_LIVES = "LIVES {0}";

    [SerializeField]
    private Button buyButton = null;
    [SerializeField]
    private Text buyCostText = null;
    [SerializeField]
    private UpgradeCellInfo upgradeLivesCell = null;

    private FiremanHealer.Data healerData;


    private void Awake()
    {
        healerData = Player.FiremansData.healerData;
        RefreshInfo();
    }


    private void OnEnable()
    {
        upgradeLivesCell.Button.onClick.AddListener(UpgradeLivesCount);
        buyButton.onClick.AddListener(BuyHealer);
    }


    private void OnDisable()
    {
        upgradeLivesCell.Button.onClick.RemoveListener(UpgradeLivesCount);
    }


    private void UpgradeLivesCount()
    {
        if (Player.TryRemoveCoins(PlayerConfig.HealerResqueLifesCost))
        {
            if (!healerData.IsBought)
            {
                throw new DataMisalignedException();
            }

            PlayerConfig.UpHealerResqueLifes();
            RefreshInfo();

            InvokeUpdateBoughtEvent();
        }
    }


    private void BuyHealer()
    {
        if (Player.TryRemoveCoins(PlayerConfig.BuyHealerCost))
        {
            PlayerConfig.BuyHealer();
            RefreshInfo();

            InvokeUpdateBoughtEvent();
        }
    }


    protected override void RefreshInfo()
    {
        buyButton.gameObject.SetActive(!healerData.IsBought);
        buyCostText.gameObject.SetActive(!healerData.IsBought);

        upgradeLivesCell.Button.gameObject.SetActive(healerData.IsBought);
        upgradeLivesCell.CostText.gameObject.SetActive(healerData.IsBought);
        upgradeLivesCell.Text.gameObject.SetActive(healerData.IsBought);

        buyCostText.text = PlayerConfig.BuyHealerCost.ToString();

        upgradeLivesCell.Text.text = string.Format(UI_FORMAT_LIVES, healerData.ResqueLives);
        upgradeLivesCell.CostText.text = PlayerConfig.BuyHealerCost.ToString();
    }
}
