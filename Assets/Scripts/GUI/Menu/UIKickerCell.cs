﻿using UnityEngine;


public class UIKickerCell : UIFiremanCell
{
    private const string UI_FORMAT_FORCE = "FORCE {0}";

    [SerializeField]
    private UpgradeCellInfo upgradeCell = null;

    private FiremanKicker.Data kickerData;


    private void Awake()
    {
        kickerData = Player.FiremansData.kickerData;
        RefreshInfo();
    }


    private void OnEnable()
    {
        upgradeCell.Button.onClick.AddListener(UpgradeKickForce);
    }


    private void OnDisable()
    {
        upgradeCell.Button.onClick.RemoveListener(UpgradeKickForce);
    }


    private void UpgradeKickForce()
    {
        if (Player.TryRemoveCoins(PlayerConfig.KickerKickForceCost))
        {
            PlayerConfig.UpKickerKickForce();
            RefreshInfo();

            InvokeUpdateBoughtEvent();
        }
    }


    protected override void RefreshInfo()
    {
        upgradeCell.Text.text = string.Format(UI_FORMAT_FORCE, kickerData.KickForce);
        upgradeCell.CostText.text = PlayerConfig.KickerKickForceCost.ToString();
    }
}
