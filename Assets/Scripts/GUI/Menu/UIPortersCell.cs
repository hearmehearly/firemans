﻿using UnityEngine;


public class UIPortersCell : UIFiremanCell
{
    private const string UI_FORMAT_SPEED = "SPEED {0}";

    [SerializeField]
    private UpgradeCellInfo upgradeSpeedCell = null;

    private FiremanPorters.Data portersData;


    private void Awake()
    {
        portersData = Player.FiremansData.portersData;
        RefreshInfo();
    }


    private void OnEnable()
    {
        upgradeSpeedCell.Button.onClick.AddListener(UpgradePortersSpeed);
    }


    private void OnDisable()
    {
        upgradeSpeedCell.Button.onClick.RemoveListener(UpgradePortersSpeed);
    }


    private void UpgradePortersSpeed()
    {
        if (Player.TryRemoveCoins(PlayerConfig.PortersSpeedCost))
        {
            PlayerConfig.UpPortersSpeed();
            RefreshInfo();

            InvokeUpdateBoughtEvent();
        }
    }


    protected override void RefreshInfo()
    {
        upgradeSpeedCell.Text.text = string.Format(UI_FORMAT_SPEED, portersData.Speed);
        upgradeSpeedCell.CostText.text = PlayerConfig.PortersSpeedCost.ToString();
    }
}