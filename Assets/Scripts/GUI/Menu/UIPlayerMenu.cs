﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class UIPlayerMenu : UITweensUnit<UnitResult>
{
    public static readonly ResourceGameObject<UIPlayerMenu> Prefab = new ResourceGameObject<UIPlayerMenu>("Game/UI/UIMenu");

    [Header("Content")]

    [SerializeField]
    private UIFiremanCell[] firemansCells = null;

    [SerializeField]
    private Text textCoins = null;
    [SerializeField]
    private Button play = null;



    private Vector2 SaveTopOffset { get; set; }

    private Vector2 SaveTopWithBannerOffset { get; set; }



    public override void Show(Action<UnitResult> onHided = null, Action onShowed = null)
    {
        base.Show(onHided, onShowed);

        play.onClick.AddListener(OnClickClose);

        RefreshVisual();

        foreach (UIFiremanCell cell in firemansCells)
        {
            cell.OnUpdateBought += RefreshVisual;
        }

        Player.OnChangeCoins += RefreshVisual;
    }


    public override void Hide(UnitResult result = null)
    {
        play.onClick.RemoveListener(OnClickClose);

        foreach (UIFiremanCell cell in firemansCells)
        {
            cell.OnUpdateBought -= RefreshVisual;
        }

        Player.OnChangeCoins -= RefreshVisual;

        base.Hide(result);
    }


    protected override void Showed()
    {
        base.Showed();
    }



    private void OnClickClose()
    {
        Hide();
    }

    private void RefreshVisual()
    {
        textCoins.text = Player.Coins.ToString();
    }
}
