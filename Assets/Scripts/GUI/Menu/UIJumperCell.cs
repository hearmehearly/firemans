﻿using UnityEngine;


public class UIJumperCell : UIFiremanCell
{
    private const string POWER_UI_FORMAT = "POWER {0}";

    [SerializeField]
    private UpgradeCellInfo upgradeCell = null;

    private FiremanJumper.Data jumperData;


    private void Awake()
    {
        jumperData = Player.FiremansData.jumperData;
        RefreshInfo();
    }


    private void OnEnable()
    {
        upgradeCell.Button.onClick.AddListener(UpgradeJumpPower);
    }


    private void OnDisable()
    {
        upgradeCell.Button.onClick.RemoveListener(UpgradeJumpPower);
    }


    private void UpgradeJumpPower()
    {
        if (Player.TryRemoveCoins(PlayerConfig.JumperJumpPowerCost))
        {
            PlayerConfig.UpJumperJumpPower();
            RefreshInfo();

            InvokeUpdateBoughtEvent();
        }
    }


    protected override void RefreshInfo()
    {
        upgradeCell.Text.text = string.Format(POWER_UI_FORMAT, jumperData.JumpPower);
        upgradeCell.CostText.text = PlayerConfig.JumperJumpPowerCost.ToString();
    }
}
