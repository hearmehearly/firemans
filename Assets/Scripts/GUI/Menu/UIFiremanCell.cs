﻿using System;
using UnityEngine;
using UnityEngine.UI;


public abstract class UIFiremanCell : MonoBehaviour
{
    public event Action OnUpdateBought;

    [Serializable]
    public class UpgradeCellInfo
    {
        public Button Button = null;
        public Text Text = null;
        public Text CostText = null;
    }


    protected abstract void RefreshInfo();


    protected virtual void InvokeUpdateBoughtEvent()
    {
        OnUpdateBought?.Invoke();
    }
}
