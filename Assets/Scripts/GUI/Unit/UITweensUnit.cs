﻿using System;
using UnityEngine;
using DG.Tweening;

[Serializable]
public class UITweenUnitData
{
    public AnimationCurve curve = null;
    public float duration = 0.7f;
    public Vector3 endVaue = Vector3.one;
}


public class UITweensUnit<T> : UIUnit<T> where T : UnitResult
{
    [Header("Animation")]

    [SerializeField]
    private Transform body = null;

    [SerializeField]
    public UITweenUnitData showData = null;
    [SerializeField]
    private UITweenUnitData hideData = null;


    public override void Show(Action<T> onHided = null, Action onShowed = null)
    {
        base.Show(onHided, onShowed);

        body.localScale = Vector3.zero;

        PlayScaleAnimation(showData, Showed);
    }

    public override void Hide(T result = null)
    {
        base.Hide(result);

        PlayScaleAnimation(hideData, () => Hided(result));
    }


    private void PlayScaleAnimation(UITweenUnitData data, TweenCallback callback)
    {
        body
            .DOScale(data.endVaue, data.duration)
            .SetEase(data.curve)
            .OnComplete(callback);
    }
}
