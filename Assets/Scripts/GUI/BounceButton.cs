﻿using UnityEngine;
using DG.Tweening;
using System;


public class BounceButton : MonoBehaviour
{
    [Serializable]
    class Step
    {
        public Vector3 endScale = Vector3.zero;
        public float scaleDuration = 0f;
    }

    [SerializeField] float sequenceDelay = 0f;
    [SerializeField] float stepsDelay = 0f;

    [SerializeField] Step[] steps = null;

    [SerializeField] RectTransform rectTransform = null;

    Vector3 startScale;

    void Start()
    {
        startScale = rectTransform.localScale;

        Sequence sequence = DOTween.Sequence();

        foreach (Step step in steps)
        {
            sequence
               .Append(transform.DOScale(step.endScale, step.scaleDuration).SetEase(Ease.Linear).SetDelay(stepsDelay).SetId(this))
               .Append(transform.DOScale(startScale, step.scaleDuration).SetEase(Ease.Linear).SetId(this));
        }

        sequence
             .SetDelay(sequenceDelay)
             .SetLoops(-1, LoopType.Restart);
    }
}
