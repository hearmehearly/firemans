﻿using UnityEngine;


public class ResourceGameObject<T> : ResourceAsset<GameObject> where T : MonoBehaviour
{
    GameObject instance;

    public T Instance
    {
        get
        {
            Instantiate();
            return instance.GetComponent<T>();
        }
    }

    public ResourceGameObject(string path) : base(path) { }

    public void Instantiate()
    {
        instance = instance ?? Object.Instantiate(Value);
    }
}
