﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class UILevel : UITweensUnit<UnitResult>
{
    public static readonly ResourceGameObject<UILevel> Prefab = new ResourceGameObject<UILevel>("Game/UI/UILevel");

    [SerializeField]
    private Text levelText = null;
    [SerializeField]
    private Text savedMansCount = null;
    [SerializeField]
    private Slider durationSlider = null;
    [SerializeField]
    private Text coinsCount = null;


    public override void Show(Action<UnitResult> onHided = null, Action onShowed = null)
    {
        base.Show(onHided, onShowed);

        SetSavedMansInfo(0); 
    }


    public void SetSavedMansInfo(int count)
    {
        savedMansCount.text = count.ToString();
    }


    public void SetLevel(int level)
    {
        levelText.text = level.ToString();
    }


    public void SetDuration(float value)
    {
        durationSlider.value = value;
    }

    public void SetCoins(float value)
    {
        coinsCount.text = value.ToString();
    }
}
