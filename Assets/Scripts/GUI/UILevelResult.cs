﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class UILevelResult : UITweensUnit<UnitResult>
{
    public static readonly ResourceGameObject<UILevelResult> Prefab = new ResourceGameObject<UILevelResult>("Game/UI/UILevelResult");

    [SerializeField]
    private Text resultReward = null;
    [SerializeField]
    private Button claimReward = null;

    private float reward;


    public void Show(Level.Result arenaResult, Action<UnitResult> onHided)
    {
        Show(onHided, null);

        reward = arenaResult.Coins;

        resultReward.text = reward.ToString();
        claimReward.onClick.AddListener(ClaimReward);
    }


    public override void Hide(UnitResult result = null)
    {
        base.Hide(result);

        claimReward.onClick.RemoveListener(ClaimReward);
    }


    public void ClaimReward()
    {
        Player.AddCoins(reward);
        Hide();
    }
}
