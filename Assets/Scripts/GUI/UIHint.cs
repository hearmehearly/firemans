﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class UIHint : UITweensUnit<UnitResult>, IPointerClickHandler, IPointerDownHandler
{
    public enum Type
    {
        None = 0,
        Press = 1,
        Hold = 2
    }


    [Serializable]
    public class SettingsInfo
    {
        public Type type = Type.None;
        public string labelText = null;
        public float noReactionCloseDelay = 0.0f;
    }


    public static readonly ResourceGameObject<UIHint> Prefab = new ResourceGameObject<UIHint>("Game/UI/UIHint");

    [SerializeField]
    private SettingsInfo[] settingsInfos = null;

    [SerializeField]
    private Text label = null;
    [SerializeField]
    private Bounce labelBounce = null;

    private Coroutine noReactionCorotine;
    private Type currentType;



    public void Show(Type hintType, Action<UnitResult> onHided, Action onShowed = null)
    {
        Show(onHided, onShowed);
        
        currentType = hintType;
        SettingsInfo currentSettings = Array.Find(settingsInfos, element => element.type == currentType);

        if (currentSettings == null)
        {
            throw new NotImplementedException("No Settings for type " + currentType);
        }

        label.text = currentSettings.labelText;
        labelBounce.Start();

        if (noReactionCorotine == null)
        {
            noReactionCorotine = Scheduler.PlayMethodWithDelay(Close, currentSettings.noReactionCloseDelay);
        }
    }


    public override void Hide(UnitResult result = null)
    {
        base.Hide(result);
        labelBounce.Finish();
    }


    private void Close()
    {
        if (noReactionCorotine != null)
        {
            Scheduler.StopPlayingCoroutine(noReactionCorotine);
            noReactionCorotine = null;
        }

        Hide();
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (currentType == Type.Press)
        {
            GameManager.Instance.UnpauseGame();
            Close();
        }
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        if (currentType == Type.Hold)
        {
            GameManager.Instance.UnpauseGame();
            Close();
        }
    }
}
