﻿using System;
using UnityEngine;


public class UnitResult { }

public class Unit<T> : MonoBehaviour where T : UnitResult
{
    private Action<T> onHided;
    private Action onShowed;

    public virtual void Show(Action<T> onHided = null, Action onShowed = null)
    {
        this.onHided = onHided ?? delegate { };
        this.onShowed = onShowed ?? delegate { };
    }

    protected virtual void Hided(T result = null)
    {
        onHided(result);
    }


    protected virtual void Showed()
    {
        onShowed();
    }
}
