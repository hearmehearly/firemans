﻿using DG.Tweening;
using UnityEngine;


public class Bounce : MonoBehaviour
{
    [SerializeField]
    private BounceSettings settings = null;

    [SerializeField]
    private bool useUnscaledDeltaTime = false;



    public void Start()
    {
        DOTween.Sequence()
        .Append(DOTween.To(() => 0f, (x) => transform.localScale = Vector3.one * settings.BounceCurve.Evaluate(x), 1f, settings.BounceCurveDuration))
        .AppendInterval(settings.BounceDelay)
        .SetLoops(-1, LoopType.Restart)
        .SetUpdate(useUnscaledDeltaTime);
    }


    public void Finish()
    {
        transform.DOKill();
    }
}
