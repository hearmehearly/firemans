﻿using UnityEngine;


[CreateAssetMenu(fileName = "BounceSettings", menuName = "Settings/BounceSettings")]
public class BounceSettings : ScriptableObject
{
    public AnimationCurve BounceCurve;
    public float BounceCurveDuration;
    public float BounceDelay;
}
