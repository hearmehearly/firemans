﻿using UnityEngine;
using DG.Tweening;


public class Bonus : MonoBehaviour
{
    public enum Type
    {
        DoubleDamage,
        Haste
    }


    private const float offsetY = 1f;

    [SerializeField]
    private float duration = 0f;
    [SerializeField]
    private float durationMove = 0f;
    [SerializeField]
    private Material hasteMaterial = null;
    [SerializeField]
    private Material doubleDamageMaterial = null;

    private MeshRenderer render;

    public Type CurrentType { get; private set; }
    public float BonusDuration { get { return duration; } }

    private void Awake()
    {
        render = GetComponent<MeshRenderer>();
    }


    private void Start()
    {
        transform.DOMove(new Vector3(transform.position.x, transform.position.y + offsetY, transform.position.z), durationMove).SetLoops(-1, LoopType.Yoyo);
	}


    public void Init(Type typeToInit)
    {
        CurrentType = typeToInit;
        render.material = typeToInit == Type.DoubleDamage ? doubleDamageMaterial : hasteMaterial;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<FiremanJumper>() != null)
        {
            Destroy(gameObject);
        }
    }
}
